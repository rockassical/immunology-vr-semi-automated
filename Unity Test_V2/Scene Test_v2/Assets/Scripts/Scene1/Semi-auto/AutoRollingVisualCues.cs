﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoRollingVisualCues : MonoBehaviour {


    public GameObject startRollingTrigger;
   // StartRolling startRollingScript;

    // Get move direction indicator materials
    public Material mUnlit;
    public Material mLit;
    public Material mLit2;
    public GameObject indicatorF;
    public GameObject indicatorB;
    public GameObject indicatorR;
    public GameObject indicatorL;
    private Renderer rF;
    private Renderer rB;
    private Renderer rR;
    private Renderer rL;

    // Get animator component
    public GameObject cockPit;
    private Animator anim;


    private void Awake()
    {
        rF = indicatorF.GetComponent<Renderer>();
        rB = indicatorB.GetComponent<Renderer>();
        rR = indicatorR.GetComponent<Renderer>();
        rL = indicatorL.GetComponent<Renderer>();

       // startRollingScript = startRollingTrigger.GetComponent<StartRolling>();
    }



    // Use this for initialization
    void Start () {

        rF.material = mUnlit;
        rB.material = mUnlit;
        rR.material = mUnlit;
        rL.material = mUnlit;

        anim = cockPit.GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {

        /*    if (startRollingScript.isRollingStart)
            {
                //Forward animation
                anim.SetBool("isForward", true);
                rF.material = mLit;
           } 

         */
    }
}
