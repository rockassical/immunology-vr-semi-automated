﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SelfDestructionCountdown : MonoBehaviour
{

    public int Minutes = 0;
    public int Seconds = 0;
    public GameObject controller;

    //public Text m_text;
    public float m_leftTime;
    private TextMeshPro timerText;
    public GameObject textMesh;

    public AudioClip teleportWarning;
    public GameObject rightController;
    AudioSource warningAudio;
    bool isOneMinLeft;

    private NETSv1 netsScript;

    public AudioManagerSemiAutoS2 audioManagerSemiAuto;
    public TimerCountdown neutrophilLifeTimer;

    public bool isTeleportTime;
    public bool isTeleportDeployed;

    private void Awake()
    {
        //m_text = GetComponent<Text>();
        m_leftTime = GetInitialTime();

        netsScript = controller.GetComponent<NETSv1>();
        timerText = textMesh.GetComponent<TextMeshPro>();
        warningAudio = rightController.GetComponent<AudioSource>();
    }

    private void Update()
    {


        if (m_leftTime > 0f && netsScript.isTriggerDown)
        {
            CompareTimers();

            //  Update countdown clock
            m_leftTime -= Time.deltaTime;
            Minutes = GetLeftMinutes();
            Seconds = GetLeftSeconds();

            timerText.text = Minutes + ":" + Seconds.ToString("00");

            if (Minutes < 1 && !isOneMinLeft && isTeleportDeployed)
            {
                isOneMinLeft = true;
                warningAudio.PlayOneShot(teleportWarning, 1f);
                //isTeleportTime = true;
            }

            if (m_leftTime <= 0f)
            {

                if (!isTeleportDeployed)
                {
                    isTeleportTime = true;
                    m_leftTime = 0f;
                    timerText.text = "0:00";
                }
                else
                {
                    m_leftTime = 0f;
                    timerText.text = "0:00";
                    StartCoroutine(audioManagerSemiAuto.FadeScreenRestart());
                }
            }


        }
    }

    private float GetInitialTime()
    {
        return Minutes * 60f + Seconds;
    }

    private int GetLeftMinutes()
    {
        return Mathf.FloorToInt(m_leftTime / 60f);
    }

    private int GetLeftSeconds()
    {
        return Mathf.FloorToInt(m_leftTime % 60f);
    }

    private float CompareTimers()
    {
        if (m_leftTime > neutrophilLifeTimer.m_leftTime)
        {
            m_leftTime = neutrophilLifeTimer.m_leftTime;

            return m_leftTime;
        }
        else
        {
            return m_leftTime;
        }
    }
}
