﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SWS;

public class SplineAddPathTest : MonoBehaviour {


    splineMove spline;
    public PathManager newPath;


    private void Awake()
    {
        spline = GetComponent<splineMove>();
    }


    // Use this for initialization
    void Start () {

        spline.SetPath(newPath);
        spline.StartMove();

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
