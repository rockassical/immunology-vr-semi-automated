﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SWS;

public class StopAutoRoute : MonoBehaviour {

    public GameObject player;
   
    public GameObject leftController;

    private splineMove splineMoveScript;
    private TouchpadMoveSemiAuto touchpadMoveSemiAuto;

    private void Awake ()
    {
        splineMoveScript = player.GetComponent<splineMove>();
        touchpadMoveSemiAuto = leftController.GetComponent<TouchpadMoveSemiAuto>();
    } 

	
    void OnTriggerEnter(Collider other)
    {
        if (splineMoveScript.enabled)
        {
            splineMoveScript.enabled = false;
            
            Debug.Log("trigger entered");
        }

        touchpadMoveSemiAuto.enabled = true;

        
    }

    void OnTriggerExit(Collider other)
    {
        if (!splineMoveScript.enabled)
        {
            return;
        }

        //Debug.Log("trigger exit");
    }
}
