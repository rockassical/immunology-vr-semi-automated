﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitTimeTrigger : MonoBehaviour {

    
    private float passedTime;
    private float triggerTime;
    public bool startCountTime;
    public float waitTime;
    public ParticleSystem particle;
    public Light pointLight;
    public GameObject directArrow;
    public GameObject directArrow2;
    public GameObject wrongDirectTrigger;
    DirectionalArrowAnimated directionalArrowAnim1;
    DirectionalArrowAnimated directionalArrowAnim2;


    private void Awake()
    {
        wrongDirectTrigger.SetActive(false);
        directionalArrowAnim1 = directArrow.GetComponent<DirectionalArrowAnimated>();
        directionalArrowAnim2 = directArrow2.GetComponent<DirectionalArrowAnimated>();
    }
    // Use this for initialization
    void Start () {
                
	}
	
	// Update is called once per frame
	void Update () {
        if (startCountTime)
        {
            passedTime = Time.time - triggerTime;
            Debug.Log("Elapsed time is: " + passedTime.ToString("F1"));
            
        }

        if(passedTime >= waitTime)
        {
            pointLight.intensity = 3f;
            particle.Play();

        }
	}

    private void OnTriggerEnter(Collider other)
    {
        triggerTime = Time.time;
        Debug.Log("Trigger time is: " + triggerTime.ToString("F1"));
        
        startCountTime = true;

        directionalArrowAnim1.startBlinking = true;
        directionalArrowAnim2.startBlinking = true;

        wrongDirectTrigger.SetActive(true);
        
        GetComponent<BoxCollider>().isTrigger = false;
    }
}
