﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthBossBacteria : MonoBehaviour {

    float lerpDuration = 10f;
    float currentLerpTime = 0f;

    public GameObject scoreTextPrefab;


    public Renderer[] rend;
    public Material[] mat;


    private Color32 deathColor = new Color32(255, 255, 255, 255);
    private Color32 liveColor;


    //private float t;
    private int healthStart = 200;
    private bool isDead;
    private bool isNetsHit;
    public int healthCurrent;



    // Use this for initialization
    void Start()
    {
        healthCurrent = healthStart;
        rend = GetComponentsInChildren<Renderer>();

    }

    private void Update()
    {
        if (isNetsHit)
        {
            currentLerpTime += Time.deltaTime;
            if (currentLerpTime > lerpDuration)
            {
                currentLerpTime = lerpDuration;
            }

            float percent = currentLerpTime / lerpDuration;


            foreach (Renderer r in rend)
            {
                liveColor = r.material.color;

                r.material.color = Color32.Lerp(liveColor, deathColor, percent);

                if (healthCurrent < 100)
                {
                    r.material.SetColor("_EmissionColor", Color.black);

                    mat = rend[1].materials;
                    mat[1].SetColor("_EmissionColor", Color.black);
                    mat[1].color = deathColor;

                    Destroy(GetComponent<Rotate>());
                    Destroy(GetComponent<Animation>());

                }

            }


        }

    }


    
    public void NetsHit()
    {
        isNetsHit = true;

        healthCurrent -= 110;
        
        GameObject scoreText = Instantiate(scoreTextPrefab, transform.position, transform.rotation);
        Destroy(scoreText, 1f);

        ScoreManager.score += 150;
        ScoreManager.killCount++;
        InflammationControl.crpAmount -= 80;

    }


    void Death()
    {

        if (healthCurrent <= 0)
        {
            isDead = true;
        }

    }
}
