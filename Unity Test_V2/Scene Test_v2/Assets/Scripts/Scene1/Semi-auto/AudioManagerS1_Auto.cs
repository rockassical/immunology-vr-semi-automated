﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SWS;


public class AudioManagerS1_Auto : MonoBehaviour {


    AudioSource guideAudio;
    public AudioClip[] aClips;
    public GameObject audioTrigger01;
    public GameObject audioTrigger02;
    public GameObject audioTrigger03;
    public GameObject audioTrigger04;
    public GameObject audioTrigger05;
    public GameObject audioTrigger06;
    public GameObject audioTrigger07;
    public GameObject audioTrigger08;
    //public bool isStartLocked;

    public bool introPlayed;
    AudioTrigger01 _audioTrigger01;
    AudioTrigger02 _audioTrigger02;
    AudioTrigger03 _audioTrigger03;
    AudioTrigger04 _audioTrigger04;
    AudioTrigger05 _audioTrigger05;
    AudioTrigger06 _audioTrigger06;
    AudioTrigger07 _audioTrigger07;
    AudioTrigger08 _audioTrigger08;


    public StartGameSemiAuto startGame;
    public Animator animLogo;

    //show cell info
    public bool showMacrophageInfo;
    public bool showPlasmaBInfo;
    public bool showNeutrophilInfo;
    public bool showSelectinInfo;

    public GameObject screen;

    private void Awake()
    {
        
        guideAudio = GetComponent<AudioSource>();

        _audioTrigger01 = audioTrigger01.GetComponent<AudioTrigger01>();
        _audioTrigger02 = audioTrigger02.GetComponent<AudioTrigger02>();
        _audioTrigger03 = audioTrigger03.GetComponent<AudioTrigger03>();
        _audioTrigger04 = audioTrigger04.GetComponent<AudioTrigger04>();
        _audioTrigger05 = audioTrigger05.GetComponent<AudioTrigger05>();
        _audioTrigger06 = audioTrigger06.GetComponent<AudioTrigger06>();
        _audioTrigger07 = audioTrigger07.GetComponent<AudioTrigger07>();
        _audioTrigger08 = audioTrigger08.GetComponent<AudioTrigger08>();



    }

    // Use this for initialization
    void Start()
    {
        
        guideAudio.PlayOneShot(aClips[0], 1f);
        introPlayed = true;
        
    }

    // Update is called once per frame
    void Update()
    {
        if(introPlayed && !guideAudio.isPlaying)
        {
            startGame.enabled = false;
            animLogo.SetTrigger("startMove");
        }
        
        if (_audioTrigger01.playAudio01 == true && !guideAudio.isPlaying)
        {
            _audioTrigger01.playAudio01 = false;
            guideAudio.PlayOneShot(aClips[1], 1f);
            
        }

        if (_audioTrigger02.playAudio02 == true && !guideAudio.isPlaying)
        {
            _audioTrigger02.playAudio02 = false;
            guideAudio.PlayOneShot(aClips[2], 1f);
            showMacrophageInfo = true;
            //StartCoroutine(ShowMacrophageInfo());
        }

        if (_audioTrigger03.playAudio03 == true && !guideAudio.isPlaying)
        {
            showMacrophageInfo = false;
            _audioTrigger03.playAudio03 = false;
            guideAudio.PlayOneShot(aClips[3], 1f);
            showPlasmaBInfo = true;
        }

        if (_audioTrigger04.playAudio04 == true && !guideAudio.isPlaying)
        {
            showPlasmaBInfo = false;
            _audioTrigger04.playAudio04 = false;
            guideAudio.PlayOneShot(aClips[4], 1f);
            //showNeutrophilInfo = true;
            StartCoroutine(ShowNeutrophilInfo());
        }

        if (_audioTrigger05.playAudio05 == true && !guideAudio.isPlaying)
        {
            
            _audioTrigger05.playAudio05 = false;
            guideAudio.PlayOneShot(aClips[5], 1f);

            StartCoroutine(ShowSelectinInfo());
        }

        if (_audioTrigger06.playAudio06 == true)
        {
            _audioTrigger06.playAudio06 = false;
            guideAudio.PlayOneShot(aClips[6], 1f);
        }

        if (_audioTrigger07.playAudio07 == true)
        {
            _audioTrigger07.playAudio07 = false;
            guideAudio.PlayOneShot(aClips[7], 1f);
        }

        if (_audioTrigger08.playAudio08 == true)
        {
            _audioTrigger08.playAudio08 = false;
            guideAudio.PlayOneShot(aClips[8], 1f);
        }
    }


//delayed bools
    IEnumerator ShowMacrophageInfo()
    {
        showMacrophageInfo = true;
        yield return new WaitForSeconds(18f);
        showMacrophageInfo = false;
    }

    IEnumerator ShowNeutrophilInfo()
    {
        showNeutrophilInfo = true;
        yield return new WaitForSeconds(30f);
        showNeutrophilInfo = false;
        screen.SetActive(false);
    }

    IEnumerator ShowSelectinInfo()
    {
        showSelectinInfo = true;
        yield return new WaitForSeconds(30f);
        showSelectinInfo = false;
        screen.SetActive(false);
    }
}
