﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using TMPro;

public class ThreeWeaponSwipeSwitch : MonoBehaviour {

    public Transform buttonAssembly;
    public int selectedWeapon = (int)Weapon.Degranulation;

    enum Weapon { Degranulation, Phagocytosis, Nets };

    public GameObject W1;
    public GameObject W2;
    public GameObject W3;

    public Material buttonUnlit;
    public Material buttonLit;

    public GameObject aimingReticle;

    public GameObject weaponPropertyBarAssembly;
    public GameObject degranulationPropertyBar;
    public GameObject phagocytosisPropertyBar;
    public GameObject netsPropertyBar;

    ParticleLauncher degranulation;
    CapsulatedIngestionV2 phagocytosis;
    NETSv1 nets;

    public TextMeshPro header;
    public TextMeshPro bodyText;



    //get touchpad swipe values
    private readonly Vector2 mXAxis = new Vector2(1, 0);
    private readonly Vector2 mYAxis = new Vector2(0, 1);
    private bool trackingSwipe = false;
    private bool checkSwipe = false;
    public bool touchPressed = false;

    // The angle range for detecting swipe
    private const float mAngleRange = 30;

    // To recognize as swipe user should at lease swipe for this many pixels
    private const float mMinSwipeDist = 0.2f;

    // To recognize as a swipe the velocity of the swipe
    // should be at least mMinVelocity
    // Reduce or increase to control the swipe speed
    private const float mMinVelocity = 4.0f;

    private Vector2 mStartPosition;
    private Vector2 endPosition;

    private float mSwipeStartTime;



    //vive controller tracking and input
    public SteamVR_TrackedObject controller;
    private SteamVR_Controller.Device device;


    private void Awake()
    {
        controller = GetComponent<SteamVR_TrackedObject>();

        degranulation = GetComponent<ParticleLauncher>();
        phagocytosis = GetComponent<CapsulatedIngestionV2>();
        nets = GetComponent<NETSv1>();

        degranulation.enabled = false;
        phagocytosis.enabled = false;
        nets.enabled = false;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        int previousSelectedWeapon = selectedWeapon;

        device = SteamVR_Controller.Input((int)controller.index);

        if (device.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad))
        {
            touchPressed = !touchPressed;

        }


        if (touchPressed)
        {
            weaponPropertyBarAssembly.SetActive(true);

            //check previously selected weapon index and turn it on
            if (selectedWeapon == (int)Weapon.Degranulation)
            {
                degranulation.enabled = true;
                W1.GetComponent<MeshRenderer>().material = buttonLit;
                aimingReticle.SetActive(true);
                degranulationPropertyBar.SetActive(true);
                header.text = "[Degranulation]";
                header.fontSize = 0.55f;
                header.color = new Color32(255, 78, 0, 255);
                bodyText.text = "Degranulation Gun Activated!";
                bodyText.color = Color.white;
            }
            else
            {
                degranulation.enabled = false;
                W1.GetComponent<MeshRenderer>().material = buttonUnlit;
                aimingReticle.SetActive(false);
                degranulationPropertyBar.SetActive(false);
            }

            if (selectedWeapon == (int)Weapon.Phagocytosis)
            {
                phagocytosis.enabled = true;
                W2.GetComponent<MeshRenderer>().material = buttonLit;
                phagocytosisPropertyBar.SetActive(true);
                header.text = "[Phagocytosis]";
                header.color = new Color32(255, 78, 0, 255);
                header.fontSize = 0.55f;
                bodyText.text = "Phagocytosis Reactor Activated!";
                bodyText.color = Color.white;
            }
            else
            {
                phagocytosis.enabled = false;
                W2.GetComponent<MeshRenderer>().material = buttonUnlit;
                phagocytosisPropertyBar.SetActive(false);
            }

            if (selectedWeapon == (int)Weapon.Nets)
            {
                nets.enabled = true;
                W3.GetComponent<MeshRenderer>().material = buttonLit;
                netsPropertyBar.SetActive(true);
                header.text = "[NETS]";
                header.color = new Color32(255, 78, 0, 255);
                header.fontSize = 0.55f;
                bodyText.text = "Neutrohpil Extracellular Traps(NETS) Activated!";
                bodyText.color = Color.white;
            }
            else
            {
                nets.enabled = false;
                W3.GetComponent<MeshRenderer>().material = buttonUnlit;
                netsPropertyBar.SetActive(false);
            }


            // Touch down, possible chance for a swipe
            if (device.GetTouchDown(SteamVR_Controller.ButtonMask.Touchpad))
            {

                trackingSwipe = true;

                // Record start time and position
                mStartPosition = device.GetAxis(EVRButtonId.k_EButton_SteamVR_Touchpad);
                mSwipeStartTime = Time.time;
            }
            // Touch up , possible chance for a swipe
            else if (device.GetTouchUp(SteamVR_Controller.ButtonMask.Touchpad))
            {
                trackingSwipe = false;
                trackingSwipe = true;
                checkSwipe = true;
                Debug.Log("Tracking Finish");
            }
            else if (trackingSwipe)
            {
                endPosition = device.GetAxis(EVRButtonId.k_EButton_SteamVR_Touchpad);

            }

            if (checkSwipe)
            {
                checkSwipe = false;

                float deltaTime = Time.time - mSwipeStartTime;

                Vector2 swipeVector = endPosition - mStartPosition;

                float velocity = swipeVector.magnitude / deltaTime;
                Debug.Log("velocity is " + velocity);

                if (velocity > mMinVelocity && swipeVector.magnitude > mMinSwipeDist)
                {

                    // if the swipe has enough velocity and enough distance
                    swipeVector.Normalize();

                    float angleOfSwipe = Vector2.Dot(swipeVector, mXAxis);
                    angleOfSwipe = Mathf.Acos(angleOfSwipe) * Mathf.Rad2Deg;

                    // Detect left and right swipe
                    if (angleOfSwipe < mAngleRange)
                    {
                        OnSwipeRight();
                    }
                    else if ((180.0f - angleOfSwipe) < mAngleRange)
                    {
                        OnSwipeLeft();
                    }
                    

                    if (previousSelectedWeapon != selectedWeapon)
                    {
                        SelectWeapon();
                    }
                }

            }
        }
        else
        {
            degranulation.enabled = false;
            phagocytosis.enabled = false;
            nets.enabled = false;
            weaponPropertyBarAssembly.SetActive(false);
            aimingReticle.SetActive(false);
            header.text = "[Mission]";
            bodyText.text = "kill bacteria, increase host health to 900 and reduce inflammation level to 150 or less";

            foreach (Transform weapon in buttonAssembly)
            {
                if (weapon.gameObject.GetComponent<MeshRenderer>() != null)
                {
                    weapon.gameObject.GetComponent<MeshRenderer>().material = buttonUnlit;
                }

            }
        }

    }


    void SelectWeapon()
    {
        int i = 0;
        foreach (Transform weapon in buttonAssembly)
        {
            if (i == selectedWeapon)
            {
                if (weapon.gameObject.GetComponent<MeshRenderer>() != null)
                {
                    weapon.gameObject.GetComponent<MeshRenderer>().material = buttonLit;
                }

            }
            else
            {
                if (weapon.gameObject.GetComponent<MeshRenderer>() != null)
                {
                    weapon.gameObject.GetComponent<MeshRenderer>().material = buttonUnlit;
                }

            }

            i++;
        }

        if (selectedWeapon == (int)Weapon.Degranulation)
        {
            degranulation.enabled = true;
            aimingReticle.SetActive(true);
            degranulationPropertyBar.SetActive(true);
            header.text = "[Degranulation]";
            header.color = new Color32(255, 78, 0, 255);
            header.fontSize = 0.55f;
            bodyText.text = "Degranulation Gun Activated!";
            bodyText.color = Color.white;
        }
        else
        {
            degranulation.enabled = false;
            aimingReticle.SetActive(false);
            degranulationPropertyBar.SetActive(false);
        }

        if (selectedWeapon == (int)Weapon.Phagocytosis)
        {
            phagocytosis.enabled = true;
            phagocytosisPropertyBar.SetActive(true);
            header.text = "[Phagocytosis]";
            header.color = new Color32(255, 78, 0, 255);
            header.fontSize = 0.55f;
            bodyText.text = "Phagocytosis Reactor Activated!";
            bodyText.color = Color.white;
        }
        else
        {
            phagocytosis.enabled = false;
            phagocytosisPropertyBar.SetActive(false);
        }

        if (selectedWeapon == (int)Weapon.Nets)
        {
            nets.enabled = true;
            netsPropertyBar.SetActive(true);
            header.text = "[NETS]";
            header.color = new Color32(255, 78, 0, 255);
            header.fontSize = 0.55f;
            bodyText.text = "Neutrohpil Extracellular Traps(NETS) Activated!";
            bodyText.color = Color.white;
        }
        else
        {
            nets.enabled = false;
            netsPropertyBar.SetActive(false);
        }
    }



    

    private void OnSwipeLeft()
    {
        Debug.Log("Swipe Left");
        
        if (selectedWeapon <= 0)
        {
            selectedWeapon = 2;
        }
        else
        {
            selectedWeapon--;
        }
    }

    private void OnSwipeRight()
    {
        Debug.Log("Swipe right");
        
        if (selectedWeapon >= 2)
        {
            selectedWeapon = 0;
        }
        else
        {
            selectedWeapon++;
        }

    }

}
