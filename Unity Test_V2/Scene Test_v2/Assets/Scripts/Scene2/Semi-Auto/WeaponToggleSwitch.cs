﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using TMPro;
using cakeslice;

enum Weapon { Degranulation, Phagocytosis, Nets };


public class WeaponToggleSwitch : MonoBehaviour {

    public Transform buttonAssembly;
    public int selectedWeapon = (int)Weapon.Nets;

    public GameObject W1;
    public GameObject W2;
    public GameObject W3;

    public Material buttonLit;
    public Material buttonUnlit;

    ParticleLauncher degranulation;
    CapsulatedIngestionV2 phagocytosis;
    NETSv1 nets;

    //public bool touchPressed = false;

    // weapon info text
    public TextMeshPro header;
    public TextMeshPro bodyText;
    public TextMeshPro animTitle;

    //weapon property graphics
    public GameObject degranulationPropertyBar;
    public GameObject phagocytosisPropertyBar;
    public GameObject netsPropertyBar;
    public GameObject weaponPropertyBarsAssembly;

    public GameObject aimingReticle;

    public bool isIngestingNow;
    public bool isNetsDepolyed;

    // play weapon animation
    public bool playDegranulationAnim;
    public bool playPhagocytosisAnim;
    public bool playNetsAnim;

    public GameObject[] bossBacteria;

    //vive controller tracking and input
    public SteamVR_TrackedObject controller;
    private SteamVR_Controller.Device device;


    private void Awake()
    {
        controller = GetComponent<SteamVR_TrackedObject>();

        degranulation = GetComponent<ParticleLauncher>();
        phagocytosis = GetComponent<CapsulatedIngestionV2>();
        nets = GetComponent<NETSv1>();
        
        degranulation.enabled = false;
        phagocytosis.enabled = false;
        nets.enabled = false;

        aimingReticle.SetActive(false);

        
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //int previousSelectedWeapon = selectedWeapon;

        device = SteamVR_Controller.Input((int)controller.index);

        if (device.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad))
        {

                if (!isIngestingNow || !isNetsDepolyed)
                {
                    OnTouchPressThreeWeapon();
                    SelectWeapon();
                }

        }

    }


    void SelectWeapon()
    {
        weaponPropertyBarsAssembly.SetActive(true);

        int i = 0;
        foreach (Transform weapon in buttonAssembly)
        {
            if (i == selectedWeapon)
            {
                if (weapon.gameObject.GetComponent<MeshRenderer>() != null)
                {
                    weapon.gameObject.GetComponent<MeshRenderer>().material = buttonLit;
                }

            }
            else
            {
                if (weapon.gameObject.GetComponent<MeshRenderer>() != null)
                {
                    weapon.gameObject.GetComponent<MeshRenderer>().material = buttonUnlit;
                }

            }

            i++;
        }

        if (selectedWeapon == (int)Weapon.Degranulation)
        {
            degranulation.enabled = true;
            DegranulationInfoText();

            degranulationPropertyBar.SetActive(true);
            aimingReticle.SetActive(true);

            playDegranulationAnim = true;
        }
        else
        {
            degranulation.enabled = false;
            aimingReticle.SetActive(false);
            degranulationPropertyBar.SetActive(false);

            playDegranulationAnim = false;
        }


        if (selectedWeapon == (int)Weapon.Phagocytosis)
        {
            phagocytosis.enabled = true;
            PhagocytosisInfoText();

            phagocytosisPropertyBar.SetActive(true);

            playPhagocytosisAnim = true;
        }
        else
        {
            phagocytosis.enabled = false;
            phagocytosisPropertyBar.SetActive(false);

            playPhagocytosisAnim = false;
        }


        if (selectedWeapon == (int)Weapon.Nets)
        {
            nets.enabled = true;
            NetsInfoText();

            netsPropertyBar.SetActive(true);

            playNetsAnim = true;

            bossBacteria = GameObject.FindGameObjectsWithTag("Bacteria Bighead");

            foreach (GameObject b in bossBacteria)
            {
                b.GetComponentInChildren<Outline>().isHit = true;
            }
        }
        else
        {
            nets.enabled = false;
            netsPropertyBar.SetActive(false);

            playNetsAnim = false;

            foreach (GameObject b in bossBacteria)
            {
                b.GetComponentInChildren<Outline>().isHit = false;
            }
        }


        
    }



    private void OnTouchPressThreeWeapon()
    {
        

        if (selectedWeapon >= 2)
        {
            selectedWeapon = 0;
        }
        else
        {
            selectedWeapon++;
        }

    }

  


    void DegranulationInfoText()
    {
        bodyText.text = "Degranulation Gun Activated!";
        header.text = "[Degranulation]";

        bodyText.color = Color.white;
        header.color = new Color32(255, 78, 0, 255);

        animTitle.text = "Degranulation";
    }

    void PhagocytosisInfoText()
    {
        bodyText.text = "Phagocytosis Reactor Activated!";
        header.text = "[Phagocytosis]";

        bodyText.color = Color.white;
        header.color = new Color32(255, 78, 0, 255);

        animTitle.text = "Phagocytosis";
    }

    void NetsInfoText()
    {
        bodyText.text = "Neutrophil Extracellular Traps(NETS) Activated! Please use it to kill highlighted boss bacteria";
        header.text = "[NETS]";

        bodyText.color = Color.white;
        header.color = new Color32(255, 78, 0, 255);
        bodyText.fontSize = 0.8f;

        animTitle.text = "NETS";
    }

}
