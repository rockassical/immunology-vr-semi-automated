﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SWS;
using TMPro;

public class StartGameSemiAuto : MonoBehaviour
{

    public GameObject logo;
    private Animator animLogo;
    private splineMove _splineMovePlayer;
    private splineMove _splineMoveFront;
    private splineMove _splineMoveBack;
    private splineMove _splineMoveSide;
    //public GameObject player;
    public GameObject frontCells;
    public GameObject backCells;
    public GameObject sideCells;
    public GameObject startText;
    private TextMeshPro t_script;
    public TimerCountdown timerScript;
    public PlayerHealth pHealthScript;
    public AudioManagerSemiAuto audioManagerScript;

    
    
    void Awake()
    {
        animLogo = logo.GetComponent<Animator>();
        _splineMovePlayer = GetComponent<splineMove>();

        _splineMoveFront = frontCells.GetComponent<splineMove>();
        _splineMoveBack = backCells.GetComponent<splineMove>();
        _splineMoveSide = sideCells.GetComponent<splineMove>();

        //isStart = _splineMove.onStart;
        t_script = startText.GetComponent<TextMeshPro>();

    }
    // Use this for initialization
    void Start()
    {
        t_script.color = new Color32(0, 143, 255, 255);
    }

    // Update is called once per frame
    void Update()
    {

        if (audioManagerScript.isStart && !audioManagerScript.guideAudio.isPlaying)
        {
            _splineMovePlayer.StartMove();

            _splineMoveFront.startPoint = 3;
            _splineMoveFront.StartMove();

            _splineMoveBack.StartMove();

            _splineMoveSide.startPoint = 2;
            _splineMoveSide.StartMove();

            t_script.color = Color.green;

            animLogo.SetTrigger("startMove");
            Destroy(logo, 3f);

            timerScript.isCountDownStart = true;
            pHealthScript.isStart = true;

            t_script.color = Color.green;

            audioManagerScript.isStart = false;
        }

    }
}
