﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoPlayControl : MonoBehaviour {

    VideoPlayer videoplayer;
    public VideoClip[] videoClips;
    public GameObject screen;
    public WeaponActivationManager weaponManager;

    private void Awake()
    {
        videoplayer = GetComponent<VideoPlayer>();
        screen.SetActive(false);
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
        if(weaponManager.playDegranulationAnim)
        {
            screen.SetActive(true);
            videoplayer.clip = videoClips[0];
            videoplayer.Play();
            weaponManager.playDegranulationAnim = false;
        }
        


        if (weaponManager.playPhagocytosisAnim)
        {
            screen.SetActive(true);
            videoplayer.clip = videoClips[1];
            videoplayer.Play();
            weaponManager.playPhagocytosisAnim = false;
        }
        

        if (weaponManager.playNetsAnim)
        {
            screen.SetActive(true);
            videoplayer.clip = videoClips[2];
            videoplayer.Play();
            weaponManager.playNetsAnim = false;
        }
        
    }
}
