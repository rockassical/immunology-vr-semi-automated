﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AudioManagerSemiAutoS2 : MonoBehaviour
{
    AudioSource guideAudio;
    public AudioClip[] audioClips;

    public GameObject leftController;
    public GameObject rightController;
    public GameObject cameraRig;
    public GameObject controllerModelR;
    public GameObject controllerModelL;

    bool isClip1Played; // degranulation intro on move [play animation]
    bool isClip2Played; // degranulation employ on stop
    bool isClip3Played; // degranulation trigger pulled "see blue particles shootin out ..."
    bool isClip4Played; // degranulation done, move on to phagocytosis
    bool isClip5Played; // phagocytosis intro [play animation]
    bool isClip6Played; // use phagocytosis, on stop
    bool isClip7Played; // ingestion visualization
    bool isClip8Played; // phagocytosis done, move on to NETS
    bool isClip9Played; // NETS deploy 
    bool isClip10Played;// another group of bac at final stop and weapon toggle
    bool isClip11Played; //teleportation reminder
    bool isClip12Played; //after teleport and mission reminder
    public bool isClip13Played; // mission accomplished
    bool isClip14Played; // start teleport
    bool isClip15Played; // NETS intro [play animation]

    public bool isDegranulationStop;
    public bool isPhagocytosisStop;
    public bool isFinalStop;
    public bool isNetsIntroPlayed;
    public bool isNetsStop;
    public bool isDegranulationDone;
    public bool isPhagocytosisDone;

    public TimerCountdown neuLifeTimer;
    public SelfDestructionCountdown destructTimer;
    public PlayerHealth health;
    public ParticleLauncher launcher;
    public CapsulatedIngestionV2 ingestion;
    public Teleportation teleport;

    public NETSv1 nets;

    // display info text
    public TextMeshPro header;
    public TextMeshPro bodyText;
    public GameObject weaponPropertyAssembly;

    //fade screen when mission done
    public GameObject creditCamera;
    public GameObject thankYouNote;
    public GameObject takeOffHeadset;

    //fade screen and mission failed note
    public GameObject missionFailed;
    public GameObject deathNoteCamera;
    public GameObject restart;
    public GameObject exit;

    //turn off selfdestruct timer when success
    public GameObject selfdestructTimer;


    private void Awake()
    {
        guideAudio = GetComponent<AudioSource>();
    }


    // Use this for initialization
    void Start()
    {
        guideAudio.clip = audioClips[0]; //intro and movement
        guideAudio.PlayDelayed(3f);

        if (!neuLifeTimer.isCountDownStart)
        {
            neuLifeTimer.isCountDownStart = true;
        }

        if (!health.isStart)
        {
            health.isStart = true;
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (!guideAudio.isPlaying && !isClip1Played)
        {
            isClip1Played = true;
            guideAudio.clip = audioClips[1]; // degranulation intro on move [play animation]
            guideAudio.PlayDelayed(5f);

        }

        if (!guideAudio.isPlaying && !isClip2Played && isDegranulationStop)
        {
            isClip2Played = true;
            guideAudio.PlayOneShot(audioClips[2], 1); // degranulation how to, on stop
        }

        if (!guideAudio.isPlaying && !isClip3Played && launcher.isLauncherPulled)
        {
            isClip3Played = true;
            guideAudio.clip = audioClips[3]; // degranulation trigger pulled "see blue particles shootin out ..."
            guideAudio.PlayDelayed(1f);
        }

        if (!guideAudio.isPlaying && !isClip4Played && isDegranulationDone)
        {
            isClip4Played = true;
            guideAudio.clip = audioClips[4]; // degranulation done, move on to phagocytosis
            guideAudio.PlayDelayed(2f);
        }


        if (!guideAudio.isPlaying && !isClip5Played && isClip4Played)
        {
            isClip5Played = true;
            guideAudio.clip = audioClips[5]; // phagocytosis intro [play animation]
            guideAudio.PlayDelayed(10);
        }


        if (!guideAudio.isPlaying && !isClip6Played && isPhagocytosisStop)
        {
            isClip6Played = true;
            guideAudio.PlayOneShot(audioClips[6], 1); // use phagocytosis, on stop
        }

        if (!guideAudio.isPlaying && !isClip7Played && ingestion.startIngestion)
        {
            isClip7Played = true;
            guideAudio.clip = audioClips[7]; // ingestion visualization
            guideAudio.PlayDelayed(2f);
        }

        if (!guideAudio.isPlaying && !isClip8Played && isPhagocytosisDone)
        {
            isClip8Played = true;
            guideAudio.clip = audioClips[8]; // phagocytosis done, move on to stop 3
            guideAudio.PlayDelayed(6f);
        }


        if (!guideAudio.isPlaying && !isClip15Played && isNetsIntroPlayed)
        {
            isClip15Played = true;
            guideAudio.PlayOneShot(audioClips[9], 1);  // NETS intro [play animation]
        }



        if (!guideAudio.isPlaying && !isClip9Played && isNetsStop)
        {
            isClip9Played = true;
            guideAudio.PlayOneShot(audioClips[10], 1); // NETS deploy 
        }


        if (!guideAudio.isPlaying && !isClip11Played && nets.isTriggerDown && destructTimer.Minutes < 1)
        {
            isClip11Played = true;
            guideAudio.PlayOneShot(audioClips[11], 1); //teleportation reminder
            
        }


        if(!guideAudio.isPlaying && !isClip14Played && isClip11Played && nets.isTriggerDown && destructTimer.m_leftTime < 5f)
        {
            isClip14Played = true;
            guideAudio.PlayOneShot(audioClips[12], 1);  // found live neutrophil, start teleporting
        }



        if (!guideAudio.isPlaying && !isClip12Played && teleport.isTeleported)
        {
            isClip12Played = true;
            guideAudio.clip = audioClips[13];
            guideAudio.PlayDelayed(3f); //after teleport and mission reminder
        }

        if(!guideAudio.isPlaying && !isClip10Played && isFinalStop)
        {
            isClip10Played = true;
            guideAudio.clip = audioClips[14]; // another group of bacteria at final stop, use touchpad toggle switch
            guideAudio.PlayDelayed(2f);
        }

        if (((int)PlayerHealth.CurrentHealth + ScoreManager.score) > 900 && InflammationControl.crpAmount <= 150f && !guideAudio.isPlaying && !isClip13Played)
        {
            isClip13Played = true;
            guideAudio.PlayOneShot(audioClips[15], 1); // mission accomplished
            header.text = "Congratulations";
            header.fontSize = 0.55f;
            header.color = Color.green;
            bodyText.text = "You have successfully controlled infection and inflammation in your host. mission accomplished!";
            bodyText.fontSize = 0.9f;
            bodyText.color = Color.green;
            weaponPropertyAssembly.SetActive(false);
            selfdestructTimer.SetActive(false);
            StartCoroutine("FadeScreen");
        }

    }



    IEnumerator FadeScreen()
    {
        yield return new WaitForSeconds(15);
        SteamVR_Fade.Start(Color.clear, 0);
        SteamVR_Fade.Start(Color.black, 2);
        yield return new WaitForSeconds(1);
        creditCamera.SetActive(true);
        yield return new WaitForSeconds(1);
        thankYouNote.SetActive(true);
        takeOffHeadset.SetActive(true);

    }



    public IEnumerator FadeScreenRestart()
    {
        yield return new WaitForSeconds(6);
        SteamVR_Fade.Start(Color.clear, 0);
        SteamVR_Fade.Start(Color.black, 2);
        yield return new WaitForSeconds(1);
        deathNoteCamera.SetActive(true);
        yield return new WaitForSeconds(2);
        missionFailed.SetActive(true);
        restart.SetActive(true);
        exit.SetActive(true);

        cameraRig.layer = LayerMask.NameToLayer("Restart");
        leftController.layer = LayerMask.NameToLayer("Restart");
        rightController.layer = LayerMask.NameToLayer("Restart");

        foreach (Transform child in leftController.transform)
        {
            child.gameObject.layer = LayerMask.NameToLayer("Restart");
        }

        foreach (Transform child in rightController.transform)
        {
            child.gameObject.layer = LayerMask.NameToLayer("Restart");
        }

        foreach (Transform child in controllerModelR.transform)
        {
            child.gameObject.layer = LayerMask.NameToLayer("Restart");
        }

        foreach (Transform child in controllerModelL.transform)
        {
            child.gameObject.layer = LayerMask.NameToLayer("Restart");
        }

        rightController.GetComponent<MagicMoveV4>().enabled = false;

        rightController.GetComponent<MenuSelection>().enabled = true;
    }
}
