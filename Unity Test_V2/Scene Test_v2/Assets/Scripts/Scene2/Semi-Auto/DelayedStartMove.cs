﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SWS;

public class DelayedStartMove : MonoBehaviour {


    splineMove spline;

    public float timeDelayed;

    private void Awake()
    {
        spline = GetComponent<splineMove>();
    }
    // Use this for initialization
    void Start () {

        StartCoroutine(DelayedStart());
	}
	
	

    IEnumerator DelayedStart()
    {
        yield return new WaitForSeconds(timeDelayed);
        spline.StartMove();
    }
}
