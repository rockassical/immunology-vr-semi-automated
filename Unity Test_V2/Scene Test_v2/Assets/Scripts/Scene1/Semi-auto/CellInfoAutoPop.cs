﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using cakeslice;



public class CellInfoAutoPop : MonoBehaviour
{

    public Material m0;
    public Material m1;
    public Material m2;
    public Material m3;
    public Material m4;
    public Material m5;
    public Material m6;
    public Material m7;
    public Material m8;
    public Material m9;

    public GameObject macrophage;
    public GameObject plasmaB;
    public GameObject neutrophil;
    public GameObject selectin;
    public GameObject[] selectins;

    public AudioManagerS1_Auto audioManagerS1;

    //cell highlight
    //Outline outlineEffect;


    //get screen position on shell
    //public GameObject eye;
    private Vector3 hitPoint;

    private Renderer rend;
    private int layerMask;
    public GameObject screen;

    void Awake()
    {

        rend = screen.GetComponent<Renderer>();

        screen.SetActive(false);
    }




    void Start()
    {
        rend.material = m0;
        layerMask = 1 << 9;
    }



    void Update()
    {

        if (audioManagerS1.showMacrophageInfo)
        {
            ShowCellInfo(macrophage.transform.position, m3);
            //StartCoroutine(ShowScreen());

            macrophage.GetComponentInChildren<Outline>().isHit = true;
            screen.SetActive(true);
        }
        else
        {
            macrophage.GetComponentInChildren<Outline>().isHit = false;
          
        }


        if (audioManagerS1.showPlasmaBInfo)
        {
            ShowCellInfo(plasmaB.transform.position, m5);

            plasmaB.GetComponentInChildren<Outline>().isHit = true;
            //screen.SetActive(true);
        }
        else
        {
            plasmaB.GetComponentInChildren<Outline>().isHit = false;
            
        }


        if (audioManagerS1.showNeutrophilInfo)
        {
            ShowCellInfo(neutrophil.transform.position, m4);
            neutrophil.GetComponentInChildren<Outline>().isHit = true;
        }
        else
        {
            neutrophil.GetComponentInChildren<Outline>().isHit = false;
            
        }



        if (audioManagerS1.showSelectinInfo)
        {
            ShowCellInfo(selectin.transform.position, m7);
            screen.SetActive(true);

            foreach(GameObject s in selectins)
            {
                s.GetComponentInChildren<Outline>().isHit = true;
            }
        }
        else
        {
            
            foreach (GameObject s in selectins)
            {
                s.GetComponentInChildren<Outline>().isHit = false;
            }
        }
        

    }


    IEnumerator ShowScreen()
    {
        
        yield return new WaitForSeconds(2);
        macrophage.GetComponentInChildren<Outline>().isHit = true;
        screen.SetActive(true);
        //yield return new WaitForSeconds(18f);
        //audioManagerS1.showMacrophageInfo = false;
    }


    void ShowCellInfo(Vector3 cellPos, Material m)
    {
        RaycastHit hitInfo;
        if (Physics.Linecast(cellPos, transform.position, out hitInfo, layerMask))
        {

            if (hitInfo.collider.tag == "Shell")
            {
                hitPoint = hitInfo.point;
                screen.transform.position = new Vector3(hitPoint.x - 0.5f, hitPoint.y, hitPoint.z);
                
                rend.material = m;

            }
        }
    }

}


