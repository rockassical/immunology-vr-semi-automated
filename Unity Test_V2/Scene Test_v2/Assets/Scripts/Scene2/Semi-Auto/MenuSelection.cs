﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using UnityEngine.SceneManagement;
using TMPro;


public class MenuSelection : MonoBehaviour {

    
    LineRenderer laser;
    public Transform endPoint;
    public TextMeshPro restart;
    bool isHit;

    public TimerCountdown timerNLife;
    public GameObject player;


    //vive controller tracking and input
    public SteamVR_TrackedObject controller;
    private SteamVR_Controller.Device device
    {
        get { return SteamVR_Controller.Input((int)controller.index); }
    }


    private void Awake()
    {
        controller = GetComponent<SteamVR_TrackedObject>();
        laser = GetComponent<LineRenderer>();
        
    }

    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {

        if (device.GetHairTrigger())
        {
            laser.enabled = true;
            laser.SetPosition(0, controller.transform.position);
            laser.SetPosition(1, endPoint.transform.position);

            int layerMask = 1 << 14;
            RaycastHit hit;
            if (Physics.Raycast(controller.transform.position, controller.transform.forward, out hit, 100f, layerMask))
            {
                if(hit.collider.tag == "Restart")
                {
                    isHit = true;
                    restart.color = Color.green;
                    
                }
                

            }
            else
            {
                restart.color = Color.white;
            }
        }
        else
        {
            laser.enabled = false;
            restart.color = Color.white;
        }


        if (device.GetHairTriggerUp() && isHit)
        {
            SceneManager.LoadScene("Scene 1 Main");
            ResetTimer();
        }

    }


    void ResetTimer()
    {
        timerNLife.m_leftTime = 900f;
        PlayerHealth.CurrentHealth = 900f;
    }

}
