﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EyeGazeTimer : MonoBehaviour {

    public GameObject canvas;
    public Image fill;
    public Slider gazeTime;
    EyeGazeInteract eyeGazeScript;


    private void Awake()
    {
        canvas.SetActive(false);
        eyeGazeScript = GetComponent<EyeGazeInteract>();
    }

    // Use this for initialization
    void Start () {
        gazeTime.value = 0f;
	}
	
	// Update is called once per frame
	void Update () {

        if (eyeGazeScript.isGazeOn)
        {
            gazeTime.value += Time.deltaTime;
            canvas.SetActive(true);
            //Debug.Log("Gaze time is " + gazeTime.value);

            if(gazeTime.value >= 1f)
            {
                canvas.SetActive(false);
               
            }
        }
        else
        {
            gazeTime.value = 0f;
            canvas.SetActive(false);
        }


	}
}
