﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using TMPro;
using UnityEngine.UI;
using SWS;

public class Teleportation : MonoBehaviour {



    public GameObject teleportTarget;

    public GameObject player;
    public GameObject neutrophilCockpitPrefab;
    public GameObject cockpit;

    public GameObject newNeutrophilBody;
    public bool isTeleported;
    public bool isTeleportChanceUsed;

    public GameObject gameTimer;
    public GameObject selfDestructionTimer;
    private TimerCountdown gameTimerScript;
    
    //public GameObject tmproDisplay;
    public TextMeshPro resetText;
    private Color32 resetTextColor = new Color32(2, 189, 251, 255);
    public TextMeshPro header;

    //private NETSv1 netsV1Script;

    public ParticleSystem teleportEffect;
   
    SelfDestructionCountdown selfDestructScript;
    splineMove splineMoveScript;
    public PathManager pathAfterTeleport;
    bool isReadyNewPath;

    public WeaponActivationManager weaponActivationManager;
    public TouchpadRotation touchpadRotation;

    //turn off animation screen
    public GameObject animScreen;

    bool isCloneGenerated;

    void Awake()
    {

        //resetText = tmproDisplay.GetComponent<TextMeshPro>();
        gameTimerScript = gameTimer.GetComponentInChildren<TimerCountdown>();
        selfDestructScript = selfDestructionTimer.GetComponentInChildren<SelfDestructionCountdown>();
        splineMoveScript = player.GetComponent<splineMove>();

    }

        // Use this for initialization
    void Start () {

    }



    private void Update()
    {
        if (selfDestructScript.isTeleportTime && !isTeleported && teleportTarget != null)
        {
            selfDestructScript.isTeleportTime = false;

            StartCoroutine(Teleport());

//            DisplayInfoReset();

            isReadyNewPath = true;

            isTeleportChanceUsed = true;   // show difference text after NETS deployed

        }

        if (isReadyNewPath)
        {
            StartCoroutine(MoveOnNewPath(8));
            isReadyNewPath = false;
        }
    }


    
    IEnumerator Teleport()
    {
        
        
        teleportEffect.Emit(100);
        yield return new WaitForSeconds(2f);

        if (!isCloneGenerated)
        {
            newNeutrophilBody = Instantiate(neutrophilCockpitPrefab, player.transform.position, player.transform.rotation);
            isCloneGenerated = true;
        }
        
        player.transform.position = teleportTarget.transform.position;
        player.transform.rotation = teleportTarget.transform.rotation;
        cockpit.transform.rotation = teleportTarget.transform.rotation;

        isTeleported = true;
        selfDestructScript.isTeleportDeployed = true;

        touchpadRotation.enabled = false;

        yield return new WaitForSeconds(1f);
        DisplayInfoReset();

        animScreen.SetActive(false);
        Destroy(teleportEffect);

  
    }

    private void DisplayInfoReset()
    {
        resetText.text = "Teleporation to another neutrophil success!";
        resetText.color = new Color32(255, 255, 255, 255);
        header.color = resetTextColor;
        header.text = "[status]";
        StartCoroutine(TextReset());

        gameTimer.SetActive(true);
        selfDestructionTimer.SetActive(false);
        gameTimerScript.m_leftTime = 360f;
    }

    IEnumerator TextReset()
    {
        yield return new WaitForSeconds(5f);
        resetText.text = "kill bacteria, increase host health to 900 and reduce inflammation level to 150 or less";
        header.text = "[mission]";
    }


    IEnumerator MoveOnNewPath(float time)
    {
        yield return new WaitForSeconds(time);
        splineMoveScript.SetPath(pathAfterTeleport);
        splineMoveScript.StartMove();
    }
}
  