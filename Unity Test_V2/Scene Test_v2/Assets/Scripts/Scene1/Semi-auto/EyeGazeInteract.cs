﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using cakeslice;
using TMPro;

public class EyeGazeInteract : MonoBehaviour
{
    public Material m0;
    public Material m1;
    public Material m2;
    public Material m3;
    public Material m4;
    public Material m5;
    public Material m6;
    public Material m7;
    public Material m8;
    public Material m9;

    //get screen position on shell
    public GameObject eye;
    private Vector3 hitPointOnShell;
    //public GameObject shell;

    private Renderer rend;

    
    //object hit by raycast
    private Vector3 hitPoint;
    public GameObject screen;

    //selection highlight
    //Collider col;
    private Outline _outline;

    public GameObject reticle;
    public bool isGazeOn;
    EyeGazeTimer gazeTimerScript;

    //List<Vector3> hitPoints = new List<Vector3>();

    public TextMeshPro t_script;
    StartGameSemiAuto startGameScript;



    void Awake()
    {

        rend = screen.GetComponent<Renderer>();

        screen.SetActive(false);

        gazeTimerScript = GetComponent<EyeGazeTimer>();

        startGameScript = GetComponent<StartGameSemiAuto>();

    }




    void Start()
    {

        
        rend.material = m0;

    }



    void Update()
    {

        SelectionHighlighting();

        RaycastHit hit;
        Debug.DrawRay(eye.transform.position, transform.forward * 200, Color.red);

        if (Physics.Raycast(eye.transform.position, transform.forward, out hit, 100))
        {


            hitPoint = hit.point;

            reticle.transform.position = hitPoint;

            int layerMask = 1 << 9;
            RaycastHit hitInfo;
            if (Physics.Linecast(hitPoint, eye.transform.position, out hitInfo, layerMask))
            {

                if (hitInfo.collider.tag == "Shell")
                {
                    hitPointOnShell = hitInfo.point;
                    //hitPoints.Add(hitInfo.point);
                    //hitPointOnShell = hitPoints[0];
                    screen.transform.position = new Vector3(hitPointOnShell.x, hitPointOnShell.y, hitPointOnShell.z);
                    //Debug.Log("scree position is " + screen.transform.position);
                }
            }


            if (hit.collider.tag == "Cytotoxic T Cell" && gazeTimerScript.gazeTime.value == 1f && isGazeOn)
            {
                screen.SetActive(true);
                rend.material = m1;
            }
            



            if (hit.collider.tag == "Helper T Cell" && gazeTimerScript.gazeTime.value == 1f && isGazeOn)
            {
                screen.SetActive(true);
                rend.material = m2;
            }



            if (hit.collider.tag == "Macrophage" && gazeTimerScript.gazeTime.value == 1f && isGazeOn)
            {
                screen.SetActive(true);
                rend.material = m3;

                Debug.Log("Macrophage hit");
            }
            



            if (hit.collider.tag == "Neutrophil" && gazeTimerScript.gazeTime.value == 1f && isGazeOn)
            {
                screen.SetActive(true);
                rend.material = m4;
            }
            



            if (hit.collider.tag == "Plasma B Cell" && gazeTimerScript.gazeTime.value == 1f && isGazeOn)
            {
                screen.SetActive(true);
                rend.material = m5;
            }
            



            if (hit.collider.tag == "Dendritic Cell")
            {
                screen.SetActive(true);
                rend.material = m6;
            }



            if (hit.collider.tag == "Selectin" && gazeTimerScript.gazeTime.value == 1f && isGazeOn)
            {
                screen.SetActive(true);
                rend.material = m7;
            }

            if (hit.collider.tag == "Red Blood Cell" && gazeTimerScript.gazeTime.value == 1f && isGazeOn)
            {
                screen.SetActive(true);
                rend.material = m8;
            }
            

            if (hit.collider.tag == "Platelet" && gazeTimerScript.gazeTime.value == 1f && isGazeOn)
            {
                screen.SetActive(true);
                rend.material = m9;
            }


            
        }
        else
        {
            //laser.SetActive(false);
            StartCoroutine(DelayedScreenOff());
            //hitPoints.Clear();
        }

    }



    
    private void SelectionHighlighting()
    {
        RaycastHit hit;
        int layer_mask = 1 << 10;

        if (Physics.Raycast(eye.transform.position, transform.forward, out hit, 200, layer_mask))
        {

            if (hit.collider.tag == "Highlightable")
            {
                //col = hit.collider;
                _outline = hit.collider.gameObject.GetComponent<Outline>();
                _outline.isHit = true;

                isGazeOn = true;

                //Debug.Log("Highlightable is hit!");
            }
            else
            {
                _outline.isHit = false;
            }

        }
        else
        {
            if (isGazeOn)
            {
                _outline.isHit = false;
            }


            isGazeOn = false;

        }
    }

    IEnumerator DelayedScreenOff()
    {
        yield return new WaitForSeconds(10f);
        screen.SetActive(false);
    }
}
