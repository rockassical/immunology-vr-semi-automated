﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using cakeslice;
using TMPro;

public class EyeGazeSemiAuto : MonoBehaviour {

    public Material m0;
    public Material m1;
    public Material m2;
    public Material m3;
    public Material m4;
    public Material m5;
    public Material m6;
    public Material m7;
    public Material m8;
    public Material m9;

    
    //show cell info
    public bool showMacrophageInfo;
    public bool showPlasmaBInfo;
    public bool showNeutrophilInfo;
    public bool showSelectinInfo;

    public GameObject macrophage;
    public GameObject plasmaB;
    public GameObject neutrophil;
    public GameObject selectinInfoPlaceHolder;
    public GameObject[] selectins;


    //get screen position on shell
    public GameObject eye;
    private Vector3 hitPointOnShell;
   
    private Renderer rend;


    //object hit by raycast
    private Vector3 hitPoint;
    public GameObject screen;

    //selection highlight
    //Collider col;
    private Outline _outline;
    private OutlineEffect outlineEffect;

    //public GameObject reticle;

    //public GameObject audioManager;
    public AudioManagerSemiAuto audioManagerScript;

    bool isMacrophageInfoShown;
    bool isPlasmaBInfoShown;
    bool isNeutrophilInfoShown;
    bool isSelectinInfoShown;

    Vector3 scaleOrig;

    public TextMeshPro t_script;

    private int layerMask;




    void Awake()
    {

        rend = screen.GetComponent<Renderer>();

        screen.SetActive(false);

        outlineEffect = GetComponent<OutlineEffect>();

    }

    


    void Start()
    {

        //reticle.SetActive(false);
        rend.material = m0;

        layerMask = 1 << 9;
    }




    void Update()
    {

        
        RaycastHit hit;
        Debug.DrawRay(eye.transform.position, transform.forward * 200, Color.red);

        if (Physics.SphereCast(eye.transform.position, 5f, eye.transform.forward, out hit, 100f))
        //if (Physics.Raycast(eye.transform.position, transform.forward, out hit, 500))
        {

         /*   
            hitPoint = hit.point;
            reticle.SetActive(true);
            

            RaycastHit hitInfo;
            if (Physics.Linecast(hitPoint, transform.position, out hitInfo, layerMask))
            {

                if (hitInfo.collider.tag == "Shell")
                {
                    Vector3 hitPoint2 = hitInfo.point;
                    reticle.transform.position = new Vector3(hitPoint2.x, hitPoint2.y, hitPoint2.z);

                }
            }

        */


            if (hit.collider.tag == "Macrophage" && audioManagerScript.isMacrophageAudioOn && !isMacrophageInfoShown)
            {
                
                StartCoroutine(ShowMacrophageInfo());
    
            }
            




            if (hit.collider.tag == "Neutrophil" && audioManagerScript.isNeutrophilAudioOn && !isNeutrophilInfoShown)
            {
                //showNeutrophilInfo = true;

                StartCoroutine(ShowNeutrophilInfo());

                
            }




            if (hit.collider.tag == "Plasma B Cell" && audioManagerScript.isPlasmaBAudioOn && !isPlasmaBInfoShown)
            {
                
                StartCoroutine(ShowPlasmaBInfo());
    
            }




            if (hit.collider.tag == "Selectin" && audioManagerScript.isSelectinAudioOn && !isSelectinInfoShown)
            {
                StartCoroutine(ShowSelectinInfo());
                
            }

/*
            if (hit.collider.tag == "Red Blood Cell" )
            {
                screen.SetActive(true);
                rend.material = m8;
            }


            if (hit.collider.tag == "Platelet" )
            {
                screen.SetActive(true);
                rend.material = m9;
            }

 */

        }


        if (showMacrophageInfo)
        {
            ShowCellInfo(macrophage.transform.position, m3);

            screen.SetActive(true);

            isMacrophageInfoShown = true;
        }


        if (showNeutrophilInfo)
        {
            ShowCellInfo(neutrophil.transform.position, m4);

            screen.SetActive(true);

            isNeutrophilInfoShown = true;
        }


        if (showPlasmaBInfo)
        {
            ShowCellInfo(plasmaB.transform.position, m5);

            screen.SetActive(true);

            isPlasmaBInfoShown = true;
        }


        
        

        if (showSelectinInfo)
        {
            //reticle.SetActive(true);
            ShowCellInfo(selectinInfoPlaceHolder.transform.position, m7);
            screen.SetActive(true);
            isSelectinInfoShown = true;
        }

    }


    void ShowCellInfo(Vector3 cellPos, Material m)
    {
        RaycastHit hitInfo;
        if (Physics.Linecast(cellPos, transform.position, out hitInfo, layerMask))
        {

            if (hitInfo.collider.tag == "Shell")
            {
                hitPoint = hitInfo.point;
                screen.transform.position = new Vector3(hitPoint.x - 0.5f, hitPoint.y, hitPoint.z);

                rend.material = m;

            }
        }
    }


/*
    private void SelectionHighlighting()
    {
        RaycastHit hit;
        int layer_mask = 1 << 10;

        if (Physics.Raycast(eye.transform.position, transform.forward, out hit, 500, layer_mask))
        {

            if (hit.collider.tag == "Highlightable")
            {
                //col = hit.collider;
                _outline = hit.collider.gameObject.GetComponent<Outline>();
                _outline.isHit = true;
                
                isGazeOn = true;

                //Debug.Log("Highlightable is hit!");
            }
            else
            {
                _outline.isHit = false;
            }

        }
        else
        {
            if (isGazeOn)
            {
                _outline.isHit = false;
            }


            isGazeOn = false;

        }
    }

*/


    


    //delayed bools
    IEnumerator ShowMacrophageInfo()
    {
        showMacrophageInfo = true;
        yield return new WaitForSeconds(18f);
        showMacrophageInfo = false;
        screen.SetActive(false);
    }

    IEnumerator ShowPlasmaBInfo()
    {
        showPlasmaBInfo = true;
        yield return new WaitForSeconds(15f);
        showPlasmaBInfo = false;
        screen.SetActive(false);
    }

    IEnumerator ShowNeutrophilInfo()
    {
        showNeutrophilInfo = true;
        yield return new WaitForSeconds(20f);
        showNeutrophilInfo = false;
        screen.SetActive(false);
        //reticle.SetActive(false);
    }

    IEnumerator ShowSelectinInfo()
    {
        showSelectinInfo = true;
        yield return new WaitForSeconds(30f);
        showSelectinInfo = false;
        screen.SetActive(false);
        //reticle.SetActive(false);
    }




    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;

        Gizmos.DrawWireSphere(eye.transform.position + eye.transform.forward * 100f, 5f);
    }


}
