﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SWS;
using TMPro;

public class WeaponActivationManager : MonoBehaviour {

    public GameObject leftController;
    public GameObject rightController;
    TouchpadRotation touchpadRotation;
    ParticleLauncher particleLauncher;
    CapsulatedIngestionV2 ingestion;
    NETSv1 nets;
    public GameObject player;
    splineMove splineMoveScript;
    
    bool degranulationDone;
    bool phagocytosisDone;
    bool killMoreBacDone;

    public int updatedScore;
    public int currentScore;

    public GameObject weaponPropertyBarAssembly;
    public GameObject degranulationPropertyBar;
    public GameObject phagocytosisPropertyBar;
    public GameObject netsPropertyBar;

    public GameObject buttonD;
    public GameObject buttonP;
    public GameObject buttonN;
    public Material buttonLit;
    public Material buttonUnlit;

    public Transform weaponButtonAssembly;
    
    Vector3 turnDirection;
    bool isMoving;
    public TextMeshPro header;
    public TextMeshPro bodyText;
    public TextMeshPro animTitle;


    //audio activation
    public AudioManagerSemiAutoS2 audioManagerSemi2;

    public GameObject aimingReticle;

    //play weapon intro animations
    public bool playDegranulationAnim;
    public bool playPhagocytosisAnim;
    public bool playNetsAnim;

    WeaponToggleSwitch weaponToggleSwitch;
    Teleportation teleport;
    ResetManager resetM;


    public GameObject finalBacteriaGroup;

    private void Awake()
    {
        particleLauncher = leftController.GetComponent<ParticleLauncher>();
        touchpadRotation = rightController.GetComponent<TouchpadRotation>();
        ingestion = leftController.GetComponent<CapsulatedIngestionV2>();
        nets = leftController.GetComponent<NETSv1>();
        splineMoveScript = player.GetComponent<splineMove>();
        aimingReticle.SetActive(false);

        weaponToggleSwitch = leftController.GetComponent<WeaponToggleSwitch>();
        weaponToggleSwitch.enabled = false;

        teleport = leftController.GetComponent<Teleportation>();
        resetM = leftController.GetComponent<ResetManager>();

        buttonUnlit = buttonD.GetComponent<Renderer>().material;

        finalBacteriaGroup.SetActive(false);
    }



    // Use this for initialization
    void Start () {

        touchpadRotation.enabled = false;
	}
	


	// Update is called once per frame
	void Update () {
		
        if(ScoreManager.killCount == 3 && degranulationDone)
        {
            
            TurnBackToRoute();
            touchpadRotation.enabled = false;
            StartCoroutine(ResumeRoute());
            particleLauncher.enabled = false;
            aimingReticle.SetActive(false);
            audioManagerSemi2.isDegranulationDone = true;
            degranulationPropertyBar.SetActive(false);
            buttonD.GetComponent<Renderer>().material = buttonUnlit;
        }

        if(ingestion.counter == 3 && phagocytosisDone)
        {
            MoveToRoute();
            touchpadRotation.enabled = false;
            StartCoroutine(ResumeRoute2());
            ingestion.enabled = false;
            audioManagerSemi2.isPhagocytosisDone = true;
            phagocytosisPropertyBar.SetActive(false);
            buttonP.GetComponent<Renderer>().material = buttonUnlit;
        }

/*
        if(ScoreManager.killCount == 10 && killMoreBacDone)
        {
            MoveToRoute();
            touchpadRotation.enabled = false;
            StartCoroutine(ResumeRoute3());
            
            particleLauncher.enabled = false;
            ingestion.enabled = false;
            weaponPropertyBarAssembly.SetActive(false);

            foreach (Transform t in weaponButtonAssembly)
            {
                t.gameObject.GetComponent<MeshRenderer>().material = buttonUnlit;
            }
        }
*/

	}



    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Degranulation Trigger")
        {
            audioManagerSemi2.isDegranulationStop = true;

            touchpadRotation.enabled = true;

            other.GetComponent<BoxCollider>().enabled = false;
            particleLauncher.enabled = true;
            aimingReticle.SetActive(true);
            splineMoveScript.Pause();

            //currentScore = ScoreManager.score;
            degranulationDone = true;

            degranulationPropertyBar.SetActive(true);

            header.text = "[DEGRANULATION]";
            header.fontSize = 0.55f;
            bodyText.text = "Degranulation Gun Activated!";

            buttonD.GetComponent<Renderer>().material = buttonLit;
        }



        if (other.tag == "Phagocytosis Trigger")
        {
            audioManagerSemi2.isPhagocytosisStop = true;

            touchpadRotation.enabled = true;

            //currentScore = ScoreManager.score;
            splineMoveScript.Pause();

            other.GetComponent<BoxCollider>().enabled = false;
            ingestion.enabled = true;

            phagocytosisDone = true;

            degranulationPropertyBar.SetActive(false);
            phagocytosisPropertyBar.SetActive(true);

            header.text = "[PHAGOCYTOSIS]";
            bodyText.text = "Phagocytosis Reactor Activated!";

            buttonP.GetComponent<Renderer>().material = buttonLit;
        }



        if (other.tag == "NETS Trigger")
        {
            audioManagerSemi2.isNetsStop = true;

            touchpadRotation.enabled = true;

            other.GetComponent<BoxCollider>().enabled = false;
            nets.enabled = true;
            
            splineMoveScript.Pause();

            weaponPropertyBarAssembly.SetActive(true);
            netsPropertyBar.SetActive(true);
            degranulationPropertyBar.SetActive(false);
            phagocytosisPropertyBar.SetActive(false);

            header.text = "[NETS]";
            bodyText.text = "Neutrohpil Extracellular Traps(NETS) Activated!";
            bodyText.fontSize = 0.8f;

            buttonN.GetComponent<Renderer>().material = buttonLit;

            finalBacteriaGroup.SetActive(true);
        }



        if(other.tag == "Final Stop")
        {
            audioManagerSemi2.isFinalStop = true;
            weaponToggleSwitch.enabled = true;
            touchpadRotation.enabled = true;
            teleport.enabled = false;
            resetM.enabled = false;
        }



        // animation triggers

        if (other.tag == "Degranulation Animation")
        {
            playDegranulationAnim = true;
            animTitle.text = "Degranulation";
            other.GetComponent<BoxCollider>().enabled = false;
            header.text = "[Degranulation]";
            header.fontSize = 0.55f;
            bodyText.text = "Introducing degranulation weapon";
        }

        if (other.tag == "Phagocytosis Animation")
        {
            playPhagocytosisAnim = true;
            animTitle.text = "Phagocytosis";
            other.GetComponent<BoxCollider>().enabled = false;
            header.text = "[Phagocytosis]";
            bodyText.text = "Introducing phagocytosis weapon";
        }

        if (other.tag == "NETS Animation")
        {
            audioManagerSemi2.isNetsIntroPlayed = true;

            playNetsAnim = true;
            animTitle.text = "NETS";
            other.GetComponent<BoxCollider>().enabled = false;
            header.text = "[NETS]";
            bodyText.text = "Introducing Neutrophil Extracelluar Traps (NETS) weapon";
            
        }

    }





    void TurnBackToRoute()
    {
        turnDirection = player.transform.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(turnDirection);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 0.5f);

    }

    void MoveToRoute()
    {
        turnDirection = player.transform.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(turnDirection);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, 25f * Time.deltaTime);

//        transform.position += (player.transform.position - transform.position).normalized * 0.3f;
/*
        if (transform.rotation == rotation)
        {
            isMoving = true;
        }

        if (isMoving)
        {
            transform.position += (player.transform.position - transform.position).normalized * 0.3f;
        }

*/
    }



    IEnumerator ResumeRoute()
    {
        yield return new WaitForSeconds(3f);
        splineMoveScript.Resume();
        degranulationDone = false;
    }


    IEnumerator ResumeRoute2()
    {
        yield return new WaitForSeconds(3f);
        splineMoveScript.Resume();
        phagocytosisDone = false;
    }


    IEnumerator ResumeRoute3()
    {
        yield return new WaitForSeconds(3f);
        splineMoveScript.Resume();
        killMoreBacDone = false;
    }
}
