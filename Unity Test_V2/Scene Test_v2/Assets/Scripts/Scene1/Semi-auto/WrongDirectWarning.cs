﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WrongDirectWarning : MonoBehaviour {

    public bool playWrongDirectAudio;
    public GameObject directArrow;
    DirectionalArrowAnimated directArrowAnim;

    private void Awake()
    {
        directArrowAnim = directArrow.GetComponent<DirectionalArrowAnimated>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            playWrongDirectAudio = true;
            directArrowAnim.startBlinking = true;
            this.gameObject.GetComponent<BoxCollider>().enabled = false;
        }
    }

}
