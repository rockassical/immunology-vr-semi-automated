﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SWS;

public class SplineMoveDelayedStart : MonoBehaviour {

    splineMove spline;

    private void Awake()
    {
        spline = GetComponent<splineMove>();
    }

    // Use this for initialization
    void Start () {

        DelayedStart();
	}
	
	IEnumerator DelayedStart()
    {
        yield return new WaitForSeconds(5f);
        spline.StartMove();
    }
}
