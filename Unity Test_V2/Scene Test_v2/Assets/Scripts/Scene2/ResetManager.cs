﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetManager : MonoBehaviour {

    
    public GameObject screen;

    private NETSv1 netsScript;
    private Teleportation teleportScript;
    

    private void Awake()
    {
        netsScript = GetComponent<NETSv1>();
        teleportScript = GetComponent<Teleportation>();
        
    }


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (teleportScript.isTeleported)
        {
            netsScript.ResetNETS();
 
            //teleportScript.isTeleported = false;
            teleportScript.enabled = false;
            netsScript.enabled = false;
            screen.SetActive(false);
        }


	}



}
