﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using TMPro;

public class NETSv1 : MonoBehaviour
{

    public GameObject player;
    public bool isTriggerDown;

    public GameObject dnaChain1a;
    public GameObject dnaChain2a;
    public GameObject dnaChain1b;
    public GameObject dnaChain2b;
    public GameObject dna1b;
    public GameObject dna2b;

    public ParticleSystem toxicParticles;
    
    private GameObject dnaChainTemp1;
    private GameObject dnaChainTemp2;
    public float speed;
    public float increment;

    public GameObject dnas;

    public GameObject gameTimer;
    public GameObject selfDestructionTimer;
    public bool isSelfDestructTimerON;

    private Vector3 originalScale;
    //public bool isReseting;

    //public GameObject textMeshAbandonShip;
    public TextMeshPro abandonShipText;
    public TextMeshPro header;

    private Teleportation teleportScript;
    private SelfDestructionCountdown selfDestructScript;

    public GameObject netsPropertyBar;
    public GameObject buttonN;
    WeaponToggleSwitch weaponToggle;
    WeaponActivationManager weaponActivationM;


    
    //vive controller tracking and input
    public SteamVR_TrackedObject controller;
    private SteamVR_Controller.Device device
    {
        get { return SteamVR_Controller.Input((int)controller.index); }
    }


    private void Awake()
    {
        controller = GetComponent<SteamVR_TrackedObject>();
        //originalScale = dnaChain1.transform.localScale;

        //abandonShipText = textMeshAbandonShip.GetComponent<TextMeshPro>();
        weaponToggle = GetComponent<WeaponToggleSwitch>();
        teleportScript = GetComponent<Teleportation>();
        selfDestructScript = selfDestructionTimer.GetComponentInChildren<SelfDestructionCountdown>();
        weaponActivationM = player.GetComponent<WeaponActivationManager>();
        
        dnaChainTemp1 = dnaChain1a;
        dnaChainTemp1.SetActive(false);

        
        dnaChainTemp2 = dnaChain2a;
        dnaChainTemp2.SetActive(false);

        originalScale = dnaChainTemp1.transform.localScale;

        dnaChain1b.SetActive(false);
        dnaChain2b.SetActive(false);
    }

 

    // Update is called once per frame
    void Update()
    {
  
        if (device.GetHairTriggerDown())
        {
            dnaChainTemp1.SetActive(true);
            dnaChainTemp2.SetActive(true);

            weaponToggle.isIngestingNow = true;
            weaponToggle.isNetsDepolyed = true;
            isTriggerDown = true;

            teleportScript.enabled = true;

            StartCoroutine(AbandonShipAlert(3));

        }


        if (isTriggerDown && speed < 160f && increment != 0f)
        {
            speed += increment;
            dnaChainTemp1.transform.localScale = originalScale * speed;
            dnaChainTemp2.transform.localScale = originalScale * speed;

            toxicParticles.Emit(1);
            InflammationControl.crpAmount += 0.05f;
            PlayerHealth.CurrentHealth -= 0.1f;
        }

    }


    public void ResetNETS()
    {
        dnaChainTemp1.transform.parent = null;
        dnaChain1a = dnaChainTemp1;
        dnaChain1a.transform.position = new Vector3 (teleportScript.newNeutrophilBody.transform.position.x - 1f, teleportScript.newNeutrophilBody.transform.position.y, teleportScript.newNeutrophilBody.transform.position.z);
        dnaChain1a.transform.rotation = teleportScript.newNeutrophilBody.transform.rotation;

        dnaChainTemp1 = dnaChain1b;
        dnaChainTemp1.transform.parent = dnas.transform;
        dnaChainTemp1.transform.position = dna1b.transform.position;
        dnaChainTemp1.transform.rotation = dna1b.transform.rotation;

        dnaChainTemp2.transform.parent = null;
        dnaChain2a = dnaChainTemp2;
        dnaChain2a.transform.position = new Vector3 (teleportScript.newNeutrophilBody.transform.position.x + 1f, teleportScript.newNeutrophilBody.transform.position.y, teleportScript.newNeutrophilBody.transform.position.z);
        dnaChain2a.transform.rotation = teleportScript.newNeutrophilBody.transform.rotation;

        dnaChainTemp2 = dnaChain2b;
        dnaChainTemp2.transform.parent = dnas.transform;
        dnaChainTemp2.transform.position = dna2b.transform.position;
        dnaChainTemp2.transform.rotation = dna2b.transform.rotation;

        speed = 0f;
        increment = 0.1f;

        dnaChainTemp1.SetActive(false);
        dnaChainTemp2.SetActive(false);

        selfDestructScript.m_leftTime = 60f;

        isTriggerDown = false;
        weaponToggle.isIngestingNow = false;
        weaponToggle.isNetsDepolyed = false;

        buttonN.GetComponent<Renderer>().material = weaponActivationM.buttonUnlit;
    }


    IEnumerator AbandonShipAlert(float time)
    {
        yield return new WaitForSeconds(time);

        netsPropertyBar.SetActive(false);
        gameTimer.SetActive(false);
        selfDestructionTimer.SetActive(true);
        isSelfDestructTimerON = true;

        if (!teleportScript.isTeleportChanceUsed)
        {
            abandonShipText.text = "Abandon neutrophil body now! System is looking for a live neutrophil to teleport.";
        }
        else
        {
            abandonShipText.text = "Your neutrophil body is dying, but you don't have a chance to teleport again. Sorry!";
        }

        abandonShipText.color = Color.red;
        header.color = Color.red;
        header.text = "[Alert]";
    }
}
