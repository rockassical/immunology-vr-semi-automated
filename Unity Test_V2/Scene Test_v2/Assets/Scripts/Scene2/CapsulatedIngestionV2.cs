﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using TMPro;

public class CapsulatedIngestionV2 : MonoBehaviour
{
    public bool isEncapsulated;
    public bool isEnemyTooBig;
    public bool isBossBacteria;

    public float alignSpeed;
    public GameObject cockpit;
    public int counter;

    public LineRenderer selectionLine;
    public Transform endPoint;
    private Transform tempEnd;
    private Vector3 lineEnd;

    public bool startIngestion;

    public GameObject capsule;
    public GameObject pivot;
    public GameObject anchor;
    public GameObject carouselAssem;

    public GameObject storageScale;
    public GameObject bacteriaCapsulePrefeb;
    public GameObject particlesInsidePrefab;

    public ParticleSystem enzymeParticles1;

    public GameObject scoreTextPrefab;

    //public GameObject textMeshInvalidIngest;
    public TextMeshPro invalidIngestWarning;
    public TextMeshPro header;

    private GameObject target;
    private float ingestDist;
    private float distOnHit;
    private Vector3 anchorOnHit;
    private Vector3 hitObjectCenter;
    private Vector3 tempPos;
    private float scaleDownRatio;
    private float scaleDifference;

    private Vector2 touchPad;

    private GameObject indicator;
    private Renderer rend;

    private RotateWithStops rotateScript;

    private AudioSource errorBeep;
    public AudioClip error;
    private bool isWrongTarget;

    private bool isMoving;
    private bool yDone;
    private bool isIngestingNow;

    WeaponToggleSwitch weaponToggleSwitch;

    public GameObject weaponPropertyBars;
    public TouchpadRotation touchpadRotation;

     
    //vive controller tracking and input
    public SteamVR_TrackedObject controller;
    private SteamVR_Controller.Device device
    {
        get { return SteamVR_Controller.Input((int)controller.index); }
    }


    private void Awake()
    {
        controller = GetComponent<SteamVR_TrackedObject>();
        capsule.SetActive(false);
        counter = 0;

        rotateScript = pivot.GetComponent<RotateWithStops>();

        //invalidIngestWarning = textMeshInvalidIngest.GetComponent<TextMeshPro>();

        tempEnd = endPoint;

        errorBeep = GetComponent<AudioSource>();

        weaponToggleSwitch = GetComponent<WeaponToggleSwitch>();
    }




    // Update is called once per frame
    void Update()
    {


        if (device.GetHairTrigger())
        {

            selectionLine.enabled = true;
            endPoint = tempEnd;
            lineEnd = endPoint.position;

            selectionLine.SetPosition(0, controller.transform.position);
            selectionLine.SetPosition(1, lineEnd);

            if (!isIngestingNow)
            {
                RaycastHit hit;
                if (Physics.Raycast(controller.transform.position, controller.transform.forward, out hit, 85f))
                {
                    Debug.DrawRay(controller.transform.position, controller.transform.forward * 20f, Color.red);

                    if (hit.collider.tag == "Bacteria" && hit.distance <= 50f)
                    {

                        endPoint = null;
                        selectionLine.SetPosition(1, hit.point);

                        target = hit.collider.gameObject;

                        hitObjectCenter = hit.collider.bounds.center;

                        //show a capsule surrounding selected bacteria
                        capsule.SetActive(true);
                        capsule.transform.position = hit.collider.bounds.center;
                        capsule.transform.localScale = hit.collider.bounds.size * 1.5f;
                        isEncapsulated = true; //turn on audio guide


                    }

                    if (hit.collider.tag == "Bacteria Bighead" && !isWrongTarget && hit.distance < 60f)
                    {
                        isWrongTarget = true;
                        StartCoroutine(WrongTargetWarningBossBacteria());
                    }


                    if (hit.collider.tag == "Bacteria Sausage" && !isWrongTarget && hit.distance <= 40f)
                    {
                        isWrongTarget = true;
                        StartCoroutine(WrongTargetWarning());
                    }

                }
                else
                {
                    lineEnd = endPoint.position;
                    isWrongTarget = false;
                }
            }
        }
        else
        {
            selectionLine.enabled = false;
            isWrongTarget = false;
        }




        if (device.GetHairTriggerUp() && target != null && target.tag == "Bacteria")
        {
            selectionLine.enabled = false;
            startIngestion = true;
            //target.tag = "Captured Bacteria";

            //lock weapon switch mech when ingestion is happening
            weaponToggleSwitch.isIngestingNow = true;
            weaponToggleSwitch.isNetsDepolyed = true;
            touchpadRotation.enabled = false;
        }


        if (startIngestion)
        {
            isIngestingNow = true;
            
            //calculate distance between bacteria and an anchor point in player
            if (target != null)
            {
                ingestDist = Vector3.Distance(target.transform.position, anchor.transform.position);
                //Debug.Log("Ingestion Dist is " + ingestDist);

            }


            if (!isMoving)
            {
                //start cockpit rotation and movement
                Quaternion r = Quaternion.LookRotation(target.transform.position - cockpit.transform.position);
                Quaternion r2 = Quaternion.Euler(cockpit.transform.rotation.x, r.eulerAngles.y, cockpit.transform.rotation.z);
                cockpit.transform.rotation = Quaternion.RotateTowards(cockpit.transform.rotation, r2, 18f * Time.deltaTime);


                if (cockpit.transform.rotation.Compare(r2, 10 ^ -3))
                {
                    isMoving = true;
                }
            }
            

            if (isMoving)
            {
                Vector3 tempTarget = target.transform.position;
                tempTarget.y = cockpit.transform.position.y - 3f;

                if (!yDone)
                {
                    target.transform.position += (tempTarget - target.transform.position).normalized * alignSpeed;
                    capsule.transform.position += (tempTarget - capsule.transform.position).normalized * alignSpeed;
                }

                if(Mathf.Abs(target.transform.position.y - tempTarget.y) <= 0.05f)
                {
                    yDone = true;

                    tempTarget.z = cockpit.transform.position.z;
                    tempTarget.x = cockpit.transform.position.x;

                    target.transform.position += (tempTarget - target.transform.position).normalized * alignSpeed;
                    capsule.transform.position += (tempTarget - capsule.transform.position).normalized * alignSpeed;
                }

                
                scaleDownRatio = 1f;

                if (target != null)
                {
                    scaleDifference = target.transform.localScale.sqrMagnitude - storageScale.transform.localScale.sqrMagnitude;
                }


                if (scaleDifference > 0f && ingestDist > 3f)
                {
                    //target.transform.localScale -= (target.transform.localScale - storageScale.transform.localScale) / distOnHit * Time.deltaTime * scaleDownRatio;
                    target.transform.localScale = Vector3.Lerp(target.transform.localScale, storageScale.transform.localScale * 1.5f, scaleDownRatio * Time.deltaTime);
                    capsule.transform.localScale = Vector3.Lerp(capsule.transform.localScale, storageScale.transform.localScale * 3.5f, scaleDownRatio * Time.deltaTime);


                    if (ingestDist <= 3f)
                    {
                        target.transform.localScale = storageScale.transform.localScale;
                        scaleDownRatio = 0f;
                    }

                }
            }


            //update ingestion dist after movement
            ingestDist = Vector3.Distance(target.transform.position, anchor.transform.position);
            


            //if distance between the bacteria and the anchor is less than a set value, the bacteria will snap to the anchor
            if (ingestDist <= 3f)
            {

                target.transform.position = anchor.transform.position;
                target.tag = "Captured Bacteria";
                //target.transform.localScale = storageScale.transform.localScale;  


                //remove bacteria rotation scripts
                Rotate[] rScripts = target.GetComponents<Rotate>();
                foreach (Rotate rot in rScripts)
                {
                    Destroy(rot);
                }

                target.transform.rotation = storageScale.transform.rotation;

                capsule.SetActive(false);

                GameObject bacteriaCapsule = Instantiate(bacteriaCapsulePrefeb, anchor.transform.position, storageScale.transform.rotation);
                bacteriaCapsule.transform.localScale = target.transform.localScale * 1.2f;


                target.transform.parent = pivot.transform;
                bacteriaCapsule.transform.parent = pivot.transform;

                Destroy(target.GetComponent<Animation>());

                counter++;

                startIngestion = false;
                touchpadRotation.enabled = true;


                //stop carousel rotation
                rotateScript.isRotating = false;

                //show particle enzyme effects
                enzymeParticles1.Play();

                GameObject particlesInside = Instantiate(particlesInsidePrefab, anchor.transform.position, target.transform.rotation);
                particlesInside.transform.localScale = storageScale.transform.localScale * 0.3f;
                particlesInside.transform.parent = bacteriaCapsule.transform;

                //show score text and increase neutrophil life (score)
                GameObject scoreText = Instantiate(scoreTextPrefab, anchor.transform.position, anchor.transform.rotation);
                Destroy(scoreText, 1f);


                ScoreManager.score += 50;
                ScoreManager.killCount++;
                InflammationControl.crpAmount -= 20f;

                yDone = false;
                isMoving = false;
                isIngestingNow = false;

                weaponToggleSwitch.isIngestingNow = false;
                weaponToggleSwitch.isNetsDepolyed = false;

            }

        }




        if (rotateScript.isRotating == true)
        {
            enzymeParticles1.Stop();
        }

    }

    /*
        IEnumerator WrongTargetWarning()
        {
            header.color = Color.red;
            header.text = "[warning]";
            invalidIngestWarning.color = Color.red;
            invalidIngestWarning.text = "Enemy is too big to be ingested!";
            isEnemyTooBig = true; //turn on warning audio
            errorBeep.PlayOneShot(error, 1f);

            yield return new WaitForSeconds(2f);
            invalidIngestWarning.color = new Color32(255, 255, 255, 255);
            invalidIngestWarning.text = "Phagocytosis in action!";
            header.color = new Color32(255, 78, 0, 255);
            header.text = "[status]";
        }
    */

    IEnumerator WrongTargetWarning()
    {
        header.color = Color.red;
        header.text = "[warning]";
        invalidIngestWarning.color = Color.red;
        invalidIngestWarning.text = "Enemy is too big to be ingested! Please use other weapon.";
        isEnemyTooBig = true; //turn on warning audio
        errorBeep.PlayOneShot(error, 1f);
        weaponPropertyBars.SetActive(false);

        yield return new WaitForSeconds(4f);
        invalidIngestWarning.color = new Color32(255, 255, 255, 255);
        invalidIngestWarning.text = "Phagocytosis in action!";
        header.color = new Color32(255, 78, 0, 255);
        header.text = "[status]";
        weaponPropertyBars.SetActive(true);
    }

    IEnumerator WrongTargetWarningBossBacteria()
    {
        header.color = Color.red;
        header.text = "[warning]";
        invalidIngestWarning.color = Color.red;
        invalidIngestWarning.text = "This gigantic bacteria can only be killed by NETS weapon.";
        isBossBacteria = true; //turn on warning audio
        errorBeep.PlayOneShot(error, 1f);
        weaponPropertyBars.SetActive(false);

        yield return new WaitForSeconds(4f);
        invalidIngestWarning.color = new Color32(255, 255, 255, 255);
        invalidIngestWarning.text = "Phagocytosis in action!";
        header.color = new Color32(255, 78, 0, 255);
        header.text = "[status]";
        weaponPropertyBars.SetActive(true);
    }

}
