﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using cakeslice;
using SWS;

public class AudioManagerSemiAuto : MonoBehaviour
{

    public AudioSource guideAudio;
    public AudioClip[] aClips;
    public GameObject audioTrigger01;
    public GameObject audioTrigger02;
    public GameObject audioTrigger03;
    public GameObject audioTrigger04;
    public GameObject audioTrigger05;
    public GameObject audioTrigger06;
    public GameObject audioTrigger07;
    public GameObject audioTrigger08;
    public bool isStart;

    bool introPlayed;
    AudioTrigger01 _audioTrigger01;
    AudioTrigger02 _audioTrigger02;
    AudioTrigger03 _audioTrigger03;
    AudioTrigger04 _audioTrigger04;
    AudioTrigger05 _audioTrigger05;
    AudioTrigger06 _audioTrigger06;
    AudioTrigger07 _audioTrigger07;
    AudioTrigger08 _audioTrigger08;

    //white blood cells to be highlighted
    public GameObject macrophage;
    public GameObject plasmaB;
    public GameObject neutrophil;
    //public GameObject selectin;
    public GameObject[] selectins;

    public GameObject screen;

    Outline outline;

    splineMove splineMoveCockpit;
    public GameObject player;

    public GameObject eye;
    EyeGazeSemiAuto eyeGazeScript;

    public bool isMacrophageAudioOn;
    public bool isPlasmaBAudioOn;
    public bool isNeutrophilAudioOn;
    public bool isSelectinAudioOn;

    public WrongDirectWarning wrongDirection;

    private void Awake()
    {


        guideAudio = GetComponent<AudioSource>();

        _audioTrigger01 = audioTrigger01.GetComponent<AudioTrigger01>();
        _audioTrigger02 = audioTrigger02.GetComponent<AudioTrigger02>();
        _audioTrigger03 = audioTrigger03.GetComponent<AudioTrigger03>();
        _audioTrigger04 = audioTrigger04.GetComponent<AudioTrigger04>();
        _audioTrigger05 = audioTrigger05.GetComponent<AudioTrigger05>();
        _audioTrigger06 = audioTrigger06.GetComponent<AudioTrigger06>();
        _audioTrigger07 = audioTrigger07.GetComponent<AudioTrigger07>();
        _audioTrigger08 = audioTrigger08.GetComponent<AudioTrigger08>();

        splineMoveCockpit = player.GetComponent<splineMove>();

        eyeGazeScript = eye.GetComponent<EyeGazeSemiAuto>();

    }

    // Use this for initialization
    void Start()
    {
            guideAudio.PlayOneShot(aClips[0], 1f);
            introPlayed = true;
        isStart = true;

    }

    // Update is called once per frame
    void Update()
    {


        if (_audioTrigger01.playAudio01 == true && !guideAudio.isPlaying)
        {
            _audioTrigger01.playAudio01 = false;
            guideAudio.PlayOneShot(aClips[1], 1f);

        }

        if (_audioTrigger02.playAudio02 == true)
        {
            eyeGazeScript.enabled = true;
            _audioTrigger02.playAudio02 = false;
            guideAudio.PlayOneShot(aClips[2], 1f);
            isMacrophageAudioOn = true;
            StartCoroutine(MacrophageHighlight());
        }

        if (_audioTrigger03.playAudio03 == true)
        {
            _audioTrigger03.playAudio03 = false;
            guideAudio.PlayOneShot(aClips[3], 1f);
            isPlasmaBAudioOn = true;
            isMacrophageAudioOn = false;
            StartCoroutine(PlasmaBHighlight());
        }

        if (_audioTrigger04.playAudio04 == true)
        {
            _audioTrigger04.playAudio04 = false;
            guideAudio.PlayOneShot(aClips[4], 1f);
            isNeutrophilAudioOn = true;
            isPlasmaBAudioOn = false;
            StartCoroutine(NeutrophilHighlight());
        }

        if (_audioTrigger05.playAudio05 == true)
        {
            _audioTrigger05.playAudio05 = false;
            guideAudio.PlayOneShot(aClips[5], 1f);
            isSelectinAudioOn = true;
            isNeutrophilAudioOn = false;
            StartCoroutine(SelectinsHighlight());
        }

        if (_audioTrigger06.playAudio06 == true)
        {
            isSelectinAudioOn = false;
            _audioTrigger06.playAudio06 = false;
            guideAudio.PlayOneShot(aClips[6], 1f);
        }

        if (_audioTrigger07.playAudio07 == true)
        {
            _audioTrigger07.playAudio07 = false;
            guideAudio.PlayOneShot(aClips[7], 1f);
        }

        if (_audioTrigger08.playAudio08 == true)
        {
            _audioTrigger08.playAudio08 = false;
            guideAudio.PlayOneShot(aClips[8], 1f);
        }

        if (wrongDirection.playWrongDirectAudio)
        {
            wrongDirection.playWrongDirectAudio = false;
            guideAudio.PlayOneShot(aClips[9], 1f);
        }
    }



    IEnumerator MacrophageHighlight()
    {
        macrophage.GetComponentInChildren<Outline>().isHit = true;
        yield return new WaitForSeconds(18);
        macrophage.GetComponentInChildren<Outline>().isHit = false;
    }

    IEnumerator PlasmaBHighlight()
    {
        plasmaB.GetComponentInChildren<Outline>().isHit = true;
        yield return new WaitForSeconds(20);
        plasmaB.GetComponentInChildren<Outline>().isHit = false;
    }

    IEnumerator NeutrophilHighlight()
    {
        neutrophil.GetComponentInChildren<Outline>().isHit = true;
        yield return new WaitForSeconds(20);
        neutrophil.GetComponentInChildren<Outline>().isHit = false;
        
    }

    IEnumerator SelectinsHighlight()
    {
        //eyeGazeScript.reticle.SetActive(true);

        foreach (GameObject s in selectins)
        {
            s.GetComponentInChildren<Outline>().isHit = true;
        }

        yield return new WaitForSeconds(30);

        foreach (GameObject s in selectins)
        {
            s.GetComponentInChildren<Outline>().isHit = false;
        }

        eyeGazeScript.enabled = false;
        
    }

}
